/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2016, Hamdi Douss
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES
 * OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.jeometry.render.awt;

import com.jeometry.geometry.twod.Shape;
import com.jeometry.geometry.twod.point.XyPoint;
import com.jeometry.geometry.twod.segment.Segment;
import com.jeometry.model.algebra.field.Field;
import com.jeometry.model.decimal.Decimal;
import java.awt.Graphics2D;

/**
 * Awt Segment painter that draws a segment on an AWT graphics.
 * @author Hamdi Douss (douss.hamdi@gmail.com)
 * @version $Id$
 * @since 0.1
 */
public final class AwtSegment extends AbstractAwtPaint {

    /**
     * Ctor.
     * @param field Field for scalar operations
     */
    public AwtSegment(final Field<Double> field) {
        super(field, Segment.class);
    }

    /**
     * Ctor.
     */
    public AwtSegment() {
        this(new Decimal());
    }

    @Override
    public void draw(final Shape renderable, final Graphics2D graphics,
        final AwtContext context) {
        final Segment seg = (Segment) renderable.renderable();
        final int width = context.width();
        final int height = context.height();
        final int scale = context.scale();
        final XyPoint start = (XyPoint) seg.start();
        final XyPoint end = (XyPoint) seg.end();
        final Double xcoor = context.center().dblx();
        final Double ycoor = context.center().dbly();
        final Double dblxstart = this.field().actual(start.xcoor());
        final Double dblystart = this.field().actual(start.ycoor());
        final Double dblxend = this.field().actual(end.xcoor());
        final Double dblyend = this.field().actual(end.ycoor());
        final int xstart = (int) (width / 2d + scale * (dblxstart - xcoor));
        final int ystart = (int) (height / 2d - scale * (dblystart - ycoor));
        final int xend = (int) (width / 2d + scale * (dblxend - xcoor));
        final int yend = (int) (height / 2d - scale * (dblyend - ycoor));
        graphics.drawLine(xstart, ystart, xend, yend);
    }

}
